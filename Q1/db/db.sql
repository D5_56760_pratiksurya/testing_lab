create table Emp(
    empid int Primary Key Auto_increment,
    name varchar(50),
    salary double,
    age int
);

insert into Emp values(0,"Ragnar",78000,35);
insert into Emp values(0,"Floki",45000,55);
insert into Emp values(0,"Dexter",80000,25);