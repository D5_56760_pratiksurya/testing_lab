const express=require("express")
const router=express.Router()
const db=require("../db")
const utils=require("../utils")


router.get("/",(request,response)=>{
    const sql=`select * from Emp`;

    db.execute(sql,(error,result)=>{
        if(error){
            response.send(utils.createResult(error))
        }
        else{   
            response.send(utils.createResult(error,result))
        }
    })
})

router.post("/add",(request,response)=>{
    const {name,salary,age}=request.body
    const sql=`insert into Emp values(0,'${name}',${salary},${age})`;

    db.execute(sql,(error,result)=>{
        if(error){
            response.send(utils.createResult(error))
        }
        else{   
            response.send(utils.createResult(error,result))
        }
    })
})

router.put("/update/:id",(request,response)=>{
    const {age}=request.body
    const {id}=request.params
    const sql=`update Emp set age=${age} where empid=${id}`;

    db.execute(sql,(error,result)=>{
        if(error){
            response.send(utils.createResult(error))
        }
        else{   
            response.send(utils.createResult(error,result))
        }
    })
})

router.delete("/delete/:id",(request,response)=>{
    const {age}=request.body
    const {id}=request.params
    const sql=`delete from Emp where empid=${id}`;

    db.execute(sql,(error,result)=>{
        if(error){
            response.send(utils.createResult(error))
        }
        else{   
            response.send(utils.createResult(error,result))
        }
    })
})


module.exports=router