const mysql=require("mysql2")

const pool=mysql.createPool({
    host:"labsql",
    user:"root",
    password:"root",
    database:"lab_test",
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0
})
module.exports=pool